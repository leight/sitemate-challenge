import React from 'react';
import {HomeView} from './HomeView';

export const HomeScreen = (): React.JSX.Element => {
  return <HomeView />;
};
