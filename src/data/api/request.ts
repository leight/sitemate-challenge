import axios, {AxiosError, AxiosRequestConfig, AxiosResponse} from 'axios';

import {ArticleData} from '../article';

export type ApiErrorData = {
  code: string;
  message: string;
  status: 'error';
};

export type ApiSuccess = {
  articles: ArticleData[];
  status: 'ok';
  totalResults: number;
};

export type ApiError = AxiosError<ApiErrorData>;

const DEFAULT_CONFIG: AxiosRequestConfig = {
  baseURL: 'https://newsapi.org/v2', // TODO: Store this in .env
};

const onSuccess = (response: AxiosResponse): ApiSuccess => {
  // Uncomment the below log to get a dump of the response data
  // console.debug('SUCCESS', {data: JSON.stringify(response?.data, null, 2)});

  return response?.data;
};

const onError = (error: ApiError) => {
  console.debug('ERROR', {
    message: error.message,
    status: error.response?.status,
  });

  return Promise.reject(error);
};

export const getRequest = async (url: string, options?: AxiosRequestConfig) => {
  const config = {
    ...DEFAULT_CONFIG,
    ...options,
    headers: {
      'Content-Type': 'application/json',
      ...(options?.headers ?? {}),
    },
  };

  console.debug(`GET "${url}"`, {
    params: config.params,
  });

  return axios
    .get(url, config)
    .then<ApiSuccess>(onSuccess)
    .catch(error => onError(error));
};
