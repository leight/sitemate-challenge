import {getRequest} from './request';

const API_KEY = '183daca270264bad86fc5b72972fb82a'; // TODO: Use .env

export const fetchTopHeadlines = () =>
  getRequest(`/top-headlines?country=us&apiKey=${API_KEY}`);

export const fetchSearchResults = (term: string) =>
  getRequest(`/everything?q=${term}&apiKey=${API_KEY}`);
