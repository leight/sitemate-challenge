import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

export const ArticlePlaceholder = (): React.JSX.Element => {
  return (
    <View style={styles.card}>
      <View style={styles.cardContent}>
        <Text style={[styles.title, styles.placeholder]}>
          This is a fake article title
        </Text>
        <Text style={[styles.cite, styles.placeholder]}>By an author</Text>
        <Text style={[styles.desc, styles.placeholder]}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
          ullamcorper purus non nunc ornare commodo eget eu orci. Vivamus
          dapibus pellentesque egestas. Nulla venenatis tempor dignissim.
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#fff',
    marginBottom: 4,
  },
  cardContent: {
    padding: 16,
  },
  placeholder: {
    color: '#eee',
    backgroundColor: '#eee',
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  cite: {
    marginTop: 4,
  },
  desc: {
    marginTop: 8,
  },
});
