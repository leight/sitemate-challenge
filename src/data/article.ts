export type ArticleData = {
  author: string;
  title: string;
  description: string | null; // null when country is `au`
  url: string;
  publishedAt: string;
};
