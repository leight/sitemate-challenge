import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {HomeScreen} from '../screens/home/HomeScreen';

const Stack = createNativeStackNavigator();

export const AppStack = (): React.JSX.Element => (
  <Stack.Navigator initialRouteName="Home">
    <Stack.Screen
      component={HomeScreen}
      name="Home"
      options={{headerTitle: 'News'}}
    />
  </Stack.Navigator>
);
