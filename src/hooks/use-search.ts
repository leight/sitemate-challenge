import {useQuery} from '@tanstack/react-query';

import {ApiError, ApiSuccess} from '../data';
import {fetchSearchResults, fetchTopHeadlines} from '../data/api/articles';

export const useSearch = (term: string | undefined) => {
  const searchTerm = (term ?? '').trim();

  const query = useQuery<unknown, ApiError, ApiSuccess>({
    queryKey: ['ARTICLE_SEARCH'],
    queryFn: () =>
      searchTerm.length ? fetchSearchResults(searchTerm) : fetchTopHeadlines(),
  });

  return {
    articles: query.data?.articles,
    error: query.error,
    isLoading: query.isFetching,
    isRefetching: query.isRefetching,
    refetch: query.refetch,
  };
};
