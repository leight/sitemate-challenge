import React from 'react';
import {Linking, Pressable, StyleSheet, Text, View} from 'react-native';

import {ArticleData} from '../../data';

type Props = {
  article: ArticleData;
};

export const Article = ({article}: Props): React.JSX.Element => {
  const onPress = () => {
    Linking.openURL(article.url);
  };

  return (
    <Pressable style={styles.card} onPress={onPress}>
      <View style={styles.cardContent}>
        <Text style={styles.title}>{article.title}</Text>
        <Text style={styles.cite}>{article.author}</Text>
        {article.description ? (
          <Text style={styles.desc}>{article.description}</Text>
        ) : undefined}
      </View>
    </Pressable>
  );
};

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#fff',
    marginBottom: 4,
  },
  cardContent: {
    padding: 16,
  },
  title: {
    color: '#383838',
    fontSize: 16,
    fontWeight: 'bold',
  },
  cite: {
    marginTop: 4,
  },
  desc: {
    marginTop: 8,
  },
});
