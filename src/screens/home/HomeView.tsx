import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, TextInput, View} from 'react-native';
import {useDebounce} from '@uidotdev/usehooks';

import {useSearch} from '../../hooks';
import {Article, ArticlePlaceholder} from '../../components/article';

export const HomeView = (): React.JSX.Element => {
  const [searchTerm, setSearchTerm] = useState('');
  const debouncedSearchTerm = useDebounce(searchTerm, 300);
  const {articles, error, isLoading, isRefetching, refetch} =
    useSearch(debouncedSearchTerm);

  const onChangeSearchTerm = (value: string) => {
    console.log({value});
    setSearchTerm(value);
  };

  useEffect(() => {
    refetch();
  }, [debouncedSearchTerm, refetch]);

  console.log({debouncedSearchTerm});

  return (
    <View style={styles.container}>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Search"
          onChangeText={onChangeSearchTerm}
        />
      </View>
      <Text style={styles.title}>
        {debouncedSearchTerm.length ? 'Search Results' : 'Top Headlines'}
      </Text>
      {isLoading ? (
        <>
          <ArticlePlaceholder />
          <ArticlePlaceholder />
          <ArticlePlaceholder />
        </>
      ) : undefined}
      {error ? <Text>Sorry, there was an error!</Text> : undefined}
      {!isLoading && !error ? (
        <>
          <FlatList
            data={articles ?? []}
            keyExtractor={(article, index) =>
              `article-${article.title}-${index}`
            }
            onRefresh={() => refetch()}
            refreshing={isRefetching}
            renderItem={({item: article}) => <Article article={article} />}
          />
        </>
      ) : undefined}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#eee',
    flex: 1,
  },
  input: {
    backgroundColor: '#fff',
    paddingHorizontal: 16,
  },
  inputContainer: {
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  title: {
    color: '#383838',
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 8,
    marginTop: 24,
    marginLeft: 16,
  },
});
